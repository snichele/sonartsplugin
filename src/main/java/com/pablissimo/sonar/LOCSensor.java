package com.pablissimo.sonar;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.batch.Sensor;
import org.sonar.api.batch.SensorContext;
import org.sonar.api.batch.fs.FileSystem;
import org.sonar.api.batch.fs.InputFile;
import org.sonar.api.config.Settings;
import org.sonar.api.measures.CoreMetrics;
import org.sonar.api.measures.FileLinesContextFactory;
import org.sonar.api.measures.Measure;
import org.sonar.api.resources.Project;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Merged from https://github.com/schnee3/SonarTsPlugin/commit/059635c4357281bf830975367ed040f36a9ca9bb
 * <p/>
 * Slightly adapted to count the number of lines too.
 * This is needed in the current sonar typescript plugin version be cause un-analized
 * files return null when runing this line
 * Measure<Integer> measure = context.getMeasure(resource, CoreMetrics.LINES);
 * in TSCoverageSensor.
 */
public class LOCSensor implements Sensor {

    private static final Logger LOG = LoggerFactory.getLogger(TsCoverageSensor.class);

    private Settings settings;
    private FileSystem fs;

    /**
     * Use of IoC to get Settings and FileSystem
     */
    public LOCSensor(FileLinesContextFactory _fileLinesContextFactory, Settings settings, FileSystem fs) {
        this.settings = settings;
        this.fs = fs;
    }

    public boolean shouldExecuteOnProject(Project project) {
        // This sensor is executed only when there are Typescript files
        return fs.hasFiles(fs.predicates().hasLanguage("ts"));
    }

    public void analyse(Project project, SensorContext sensorContext) {
        LOG.info("LOCSensor Analyzing...");
        // This sensor count the Line of source code in every .ts file

        for (InputFile inputFile : fs.inputFiles(fs.predicates().hasLanguage("ts"))) {
            LOG.info("Number of lines for " + inputFile);
            double lines = 0;
            try {
                lines = totalLines(inputFile);
                LOG.info("- total lines : " + lines);
            } catch (Exception e) {
                e.printStackTrace();
            }
            double value = this.getNumberCodeLine(inputFile);
            LOG.info("- lines of code : " + value);

            sensorContext.saveMeasure(inputFile, new Measure<Integer>(CoreMetrics.LINES, lines));
            sensorContext.saveMeasure(inputFile, new Measure<Integer>(CoreMetrics.NCLOC, value));
        }
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }

    private double getNumberCodeLine(InputFile inputFile) {
        double value = 0;
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(inputFile.file()));

            boolean isEOF = false;
            boolean isCommentOpen = false;
            boolean isACommentLine = false;
            do {

                String line = br.readLine();
                if (line != null) {
                    isACommentLine = false;
                    line = line.trim();

                    if (isCommentOpen) {
                        if (line.contains("*/")) {
                            isCommentOpen = false;
                            isACommentLine = true;
                        }
                    } else {
                        if (line.startsWith("//")) {
                            isACommentLine = true;
                        }
                        if (line.startsWith("/*")) {
                            if (line.contains("*/")) {
                                isCommentOpen = false;
                            } else {
                                isCommentOpen = true;
                            }
                            isACommentLine = true;

                        } else if (line.contains("/*")) {
                            if (line.contains("*/")) {
                                isCommentOpen = false;
                            } else {
                                isCommentOpen = true;
                            }
                            isACommentLine = false;
                        }
                    }
                    isEOF = true;
                    line = line.replaceAll("\\n|\\t|\\s", "");
                    if ((!line.equals("")) && !isACommentLine) {
                        value++;
                    }
                } else {
                    isEOF = false;
                }
            } while (isEOF);

            br.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return value;
    }

    private double totalLines(InputFile inputFile) throws Exception {
        BufferedReader reader = new BufferedReader(new FileReader(inputFile.file()));
        int lines = 0;
        while (reader.readLine() != null) lines++;
        reader.close();
        return lines;
    }
}
